### Introduction

This is a sample flask app with Dockerfile and kubernetes specs.

### How to run

1. Build the image using command `docker build -t k8s-hello-world .`
2. Push this image to an image hosting service. If you want to use Google Cloud Regristry , then refer [this documentation](https://cloud.google.com/container-registry/docs/pushing-and-pulling). Also if you want a test accpount to try on GCR please contact [Tushar](https://teams.microsoft.com/l/chat/0/0?users=Tushar@smarter.codes)  or [Maneesh](https://teams.microsoft.com/l/chat/0/0?users=maneesh.s@smarter.codes) 
    Also if you are using GCR make sure to set container visibility to "Public" 
   ![MicrosoftTeams-image__1_](/uploads/bd6ceef72492a6d3a3b8673594ff3537/MicrosoftTeams-image__1_.png)
3. Set up minikube/kubernetes cluster.
4. Run `kubectl apply -f deployment.yaml` to create deployment.
5. Run `kubectl apply -f service.yaml` to create service.
