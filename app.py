import os
from flask import Flask      # Import flask

app = Flask(__name__)        # Create a flask instance

@app.route('/')              # Create a route
def hello_world():
    return "Hello World"

# Run flask server
if __name__ == '__main__':
   app.run(host=os.getenv('HOST'), port=int(os.getenv('PORT')))
